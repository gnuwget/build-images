FROM debian:unstable

LABEL maintainer "Tim Rühsen <tim.ruehsen@gmx.de>"

WORKDIR /usr/local

RUN apt-get update -y && apt-get upgrade -y && apt-get install -y apt-utils
RUN apt-get install -y \
	pkg-config \
	gcc \
	g++ \
	make \
	gettext \
	autopoint \
	git \
	autoconf \
	autoconf-archive \
	automake \
	libtool \
	texinfo \
	flex \
	gperf \
	libidn2-dev \
	libunistring-dev \
	zlib1g-dev \
	libbz2-dev \
	liblzma-dev \
	libbrotli-dev \
	libzstd-dev \
	gnutls-dev \
	libgcrypt-dev \
	nettle-dev \
	libpsl-dev \
	libnghttp2-dev \
	libpcre2-dev \
	lcov \
	doxygen \
	openssl \
	libssl-dev \
	libwolfssl-dev \
	texlive \
	valgrind \
	lzip \
	graphviz \
	wget \
	pandoc \
	libgpgme-dev \
	ccache \
	python-is-python3


RUN git clone --recursive https://gitlab.com/gnuwget/gnulib-mirror.git gnulib
ENV GNULIB_SRCDIR /usr/local/gnulib
ENV GNULIB_TOOL /usr/local/gnulib/gnulib-tool

RUN git clone --depth 1 https://github.com/maru/libmicrohttpd-http2.git
RUN cd libmicrohttpd-http2/ \
       	&& ./bootstrap \
       	&& ./configure --enable-http2 --prefix=/usr \ 
	&& make && make install
