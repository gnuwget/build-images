#!/bin/sh

image=$1
startdir=$2
registry=$3
project=$4

master_build=0
if test "$CI_PROJECT_NAMESPACE" = "gnuwget" && test "$CI_PROJECT_NAME" = "build-images"; then
	master_build=1
fi

if test $master_build = 0; then
	echo Image is already up-to-date
	exit 0
fi

set -e
echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" "$registry" --password-stdin
docker build -t "$registry/$project:$image" "$startdir"
docker push "$registry/$project:$image"

exit 0
