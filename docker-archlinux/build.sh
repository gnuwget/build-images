#!/usr/bin/env -S bash -eu

SRC=${SRC:-/usr/local}

export WGET2_DEPS_PATH=$SRC/wget2_deps
export PKG_CONFIG_PATH=$WGET2_DEPS_PATH/lib/pkgconfig
export CPPFLAGS="-I$WGET2_DEPS_PATH/include"
export LDFLAGS="-L$WGET2_DEPS_PATH/lib"
export GNULIB_SRCDIR=$SRC/gnulib
export GNULIB_TOOL=$GNULIB_SRCDIR/gnulib-tool

#cd $SRC/libunistring
#./autogen.sh
#./configure --prefix=$WGET2_DEPS_PATH
#make -j$(nproc)
#make install
#ldconfig

#cd $SRC/libidn2
#./bootstrap
#./configure --disable-doc --disable-gcc-warnings --prefix=$WGET2_DEPS_PATH
#make -j$(nproc)
#make install
#ldconfig

#cd $SRC/libpsl
#./autogen.sh
#./configure --disable-gtk-doc --enable-runtime=libidn2 --enable-builtin=libidn2 --prefix=$WGET2_DEPS_PATH
#make -j$(nproc)
#make install
#ldconfig

# We could use GMP from git repository to avoid false positives in
# sanitizers, but GMP doesn't compile with clang. We use gmp-mini
# instead.
#cd $SRC/nettle
#bash .bootstrap
#./configure --enable-mini-gmp --disable-documentation --prefix=$WGET2_DEPS_PATH
#( make -j$(nproc) || make -j$(nproc) ) && make install
#if test $? != 0;then
#        echo "Failed to compile nettle"
#        exit 1
#fi
#ldconfig

#cd $SRC/gnutls
#./bootstrap
#./configure --with-nettle-mini --disable-gcc-warnings --with-included-libtasn1 \
#    --with-included-unistring --without-p11-kit --disable-doc --disable-tests --disable-tools --disable-cxx \
#    --disable-maintainer-mode --disable-libdane --disable-gcc-warnings --prefix=$WGET2_DEPS_PATH --disable-guile
#make -j$(nproc)
#make install
#ldconfig

#cd $SRC/libmicrohttpd-*
#./configure --prefix=$WGET2_DEPS_PATH --disable-doc --disable-examples
#make -j$(nproc)
#make install
#ldconfig
