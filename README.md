# Wget -- Building CI images

This Wget sub-project generates and pushes to gitlab.com
docker registry the docker images to be used for compiling the
master branch of Wget2.

The reason for pre-generating the images is to speed-up CI runs
and avoid failures due to downloading of individual packages (e.g.,
because some mirrors were down).


# How to generate a new image

Add a new directory with a Dockerfile containing the instructions
for the image.

Then edit .gitlab-ci.yml to add the build instructions, commit and push.


# How to re-generate an existing image

Visit the [container registry page](https://gitlab.com/gnuwget/build-images/container_registry) of the
Gitlab project. Then
 * delete the old image (at this point no builds are possible with that image)
 * Push in the build-images repository

The image will be re-build.

# Building locally (example for buildenv-debian-stable)
```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/gnuwget/build-images:buildenv-debian-stable docker-debian-stable
docker push registry.gitlab.com/gnuwget/build-images:buildenv-debian-stable
```

# Build and test outside Gitlab registry
```
$ docker build docker-debian-mingw
...
Successfully built 54b10afffc6c
$ docker run -it 54b10afffc6c /bin/bash
# git clone https://gitlab.com/gnuwget/wget2.git
# cd wget2
```
